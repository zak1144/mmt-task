import React, { Component } from "react";

import Item from "./Item";
import BasketTotal from './BasketTotal';

class App extends Component {
  state = {
    basketItems: [
      {
        name: "Mountain Dew",
        totalUnits: 2,
        singleUnitPrice: 1.8,
        id: 1
      },
      {
        name: "Desperados",
        totalUnits: 6,
        singleUnitPrice: 2.583,
        id: 2
      },
      {
        name: "Jack Daniels",
        totalUnits: 4,
        singleUnitPrice: 3.35,
        id: 3
      }
    ]
  };
  
  handleQuantity = index => e => {
     let newQuantity = parseInt(e.target.value);
     let { basketItems } = this.state;
     basketItems[index].totalUnits=newQuantity;
      this.setState({
         basketItems
      });
  };
  handleRemoveItem = id => {
    this.setState(prevState => {
      return {
        basketItems: prevState.basketItems.filter(itm => itm.id !== id)
      };
    });
  };
  handleClear=()=>{
     let { basketItems } = this.state;
     basketItems.map(item=>item.totalUnits=0);
      this.setState({
         basketItems
      });
  }

  render() {
    return (
      <div className="basket">
        <div className="inner">
          {this.state.basketItems.map((item, i) => (
            <Item
              key={i}
              name={item.name}
              totalUnits={item.totalUnits}
              singleUnitPrice={item.singleUnitPrice}
              changeQuantity={this.handleQuantity}
              index={i}
              id={item.id}
              removeItem={this.handleRemoveItem}
            />
          ))}
          <BasketTotal items={this.state.basketItems} handleClear={this.handleClear} />
        </div>
      </div>
    );
  }
}

export default App;
