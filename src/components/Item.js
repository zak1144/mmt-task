import React, { Component } from "react";

class Item extends Component {
  render() {
    const {
      name,
      totalUnits,
      changeQuantity,
      singleUnitPrice,
      removeItem,
      id,
      index
    } = this.props;

    return (
      <div className="item">
        <div className="item-name-no">
          <span className="item-name">{name}</span>
          <input
            type="number"
            placeholder="no:"
            size="2"
            maxLength="1"
            value={totalUnits ? totalUnits : 0}
            onChange={changeQuantity(index)}
          />
        </div>
        <div className="cost-btn">
          <span className="item-cost">${(totalUnits*singleUnitPrice).toFixed(2)}</span>
          <button className="remove-item" onClick={() => removeItem(id)}>
            ✖
          </button>
        </div>
      </div>
    );
  }
}

export default Item;
