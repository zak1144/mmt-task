import React from "react";

const BasketTotal = props => {
   console.log('props', props);
  const totalCost = props.items.reduce((acc, item) => acc + item.totalUnits*item.singleUnitPrice, 0);
  return (
    <table>
      <tbody>
        <tr>
          <td>$ {totalCost.toFixed(2)}</td>
          <td ><button className='clearBtn' onClick={props.handleClear}>Clear</button></td>
          <td>
            <button className='checkoutBtn' >
              Check Out
              <i className="arrow" />
            </button>
          </td>
        </tr>
      </tbody>
    </table>
  );
};

export default BasketTotal;
